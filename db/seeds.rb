# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'faker'
10.times do
	Student.create!(name: Faker::Name.name, birth_date: Faker::Date.between("1991-01-01", Date.today), avatar: Faker::Avatar.image)
end