json.result do
  json.messages "ok"
  json.rstatus  "1"
  json.errorcode ""
end
json.data do
  json.id @student.id
  json.name @student.name
  json.birth_date @student.birth_date
  json.avatar @student.avatar
end